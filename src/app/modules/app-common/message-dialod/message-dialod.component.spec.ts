import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageDialodComponent } from './message-dialod.component';

describe('MessageDialodComponent', () => {
  let component: MessageDialodComponent;
  let fixture: ComponentFixture<MessageDialodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageDialodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageDialodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
