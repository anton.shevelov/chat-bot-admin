import { Component, OnInit, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, FormControl } from '@angular/forms';
import { NgModelComponent } from '../ng-model/ng-model.component';

@Component({
    selector: 'app-input',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: InputComponent,
            multi: true,
        },
    ],
})
export class InputComponent extends NgModelComponent implements OnInit {
    @Input() placeholder = '';
    @Input() fieldName = '';
    @Input() field: FormControl;
    @Input() type: string;
    @Input() errorMessage = '';
    @Input() hint = '';
    @Input() submited = false;

    public isFieldFocused = false;

    get isFieldValid() {
        return !this.isFieldFocused && this.submited && this.field.invalid;
    }

    constructor() {
        super();
    }

    ngOnInit(): void {
    }
}
