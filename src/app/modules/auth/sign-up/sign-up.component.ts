import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthRoutes } from '../auth-routes';
import { EMAIL_REGEXP } from '@const/reg-exp';

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.scss', '../common-styles.scss'],
})
export class SignUpComponent implements OnInit, OnDestroy {
    public signUpForm: FormGroup;
    public submited = false;

    constructor(public authRoutes: AuthRoutes, private fb: FormBuilder) {}

    ngOnInit(): void {
        this.signUpForm = this.fb.group({
            email: [
                null,
                [Validators.required, Validators.pattern(EMAIL_REGEXP)],
            ],
            login: [null, [Validators.required, Validators.minLength(5)]],
            password: [null, [Validators.required, Validators.minLength(8)]],
        });
    }

    onSubmit(f: FormGroup): void {
        this.submited = true;
        if (f.invalid) {
            return;
        }
    }

    close() {
        this.authRoutes.closeAuthPage();
    }

    ngOnDestroy(): void {
        this.signUpForm.reset();
    }
}
