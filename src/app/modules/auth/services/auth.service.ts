import { Injectable } from '@angular/core';
import { HttpService } from '@services/http.service';

import { Urls } from '@const/urls';
import { Login } from '@interface/login';
import { Auth } from '@interface/auth';
import { AUTH_STORE_KEY } from '@const/store-const';
import { Router } from '@angular/router';
import { AdminRoutes } from '@modules/admin/admin-routes';
import { AuthRoutes } from '../auth-routes';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    constructor(
        private http: HttpService,
        private urls: Urls,
        private router: Router,
        private adminRoutes: AdminRoutes,
        private authRoutes: AuthRoutes
    ) {}

    login(loginData: Login, errCallback: () => void): void {
        this.http.post(this.urls.LOGIN, loginData).subscribe(
            (credentials: Auth) => {
                this.saveCredentials(credentials);
                this.router.navigate([this.adminRoutes.PRODUCTS]);
            },
            (loginError) => errCallback()
        );
    }
    saveCredentials(credentials: Auth): void {
        window.localStorage.setItem(
            AUTH_STORE_KEY,
            JSON.stringify(credentials)
        );
    }
    getCredentials(): Auth | null {
        const storedCredentials = window.localStorage.getItem(AUTH_STORE_KEY);
        if (storedCredentials) {
            return JSON.parse(storedCredentials);
        }
        return null;
    }
    logout(): void {
        window.localStorage.clear();
        window.sessionStorage.clear();
        this.router.navigate([this.authRoutes.SIGN_IN]);
    }
}
