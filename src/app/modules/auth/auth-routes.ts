import { Router } from '@angular/router';
import { AppRoutes } from '@modules/app-core/app-routes';
import { Injectable } from '@angular/core';

export const AUTH = 'auth';
export const SIGN_IN = 'sign-in';
export const SIGN_UP = 'sign-up';
export const SIGN_UP_COMPLETE = 'registration-complete';

@Injectable({
    providedIn: 'root',
})
export class AuthRoutes {
    public SIGN_IN = `/${AUTH}/${SIGN_IN}`;
    public SIGN_UP = `/${AUTH}/${SIGN_UP}`;
    public SIGN_UP_COMPLETE = `/${AUTH}/${SIGN_UP_COMPLETE}`;
    constructor(private router: Router, private appRoutes: AppRoutes) {}
    closeAuthPage(): void {
        this.router.navigate([this.appRoutes.START]);
    }
}
