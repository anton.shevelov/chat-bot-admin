import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { SpinnerAction, MessageDialog } from '@actions';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
    public spinner: Observable<SpinnerAction>;
    public message: Observable<MessageDialog>;
    constructor(private store: Store<any>) {}
    ngOnInit(): void {
        this.spinner = this.store.pipe(select('spinner'));
        this.message = this.store.pipe(select('message'));
    }
}
