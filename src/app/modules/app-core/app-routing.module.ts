import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AUTH } from '@modules/auth/auth-routes';
import { AuthRouterComponent } from '../auth/auth-router/auth-router.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PAGE_NOT_FOUND, START } from './app-routes';
import { StartPageComponent } from '@modules/app-common/start-page/start-page.component';
import { AdminRouterComponent } from '@modules/admin/components/admin-router/admin-router.component';
import { ADMIN } from '@modules/admin/admin-routes';
import { AuthGuard } from '@modules/auth/guards/auth.guard';

const routes: Routes = [
    { path: '', redirectTo: AUTH, pathMatch: 'full' },
    {
        path: START,
        component: StartPageComponent,
    },
    {
        path: AUTH,
        component: AuthRouterComponent,
        loadChildren: () =>
            import('../auth/auth.module').then(m => m.AuthModule),
    },
    {
        path: ADMIN,
        component: AdminRouterComponent,
        loadChildren: () =>
            import('../admin/admin.module').then(m => m.AdminModule),
        canActivate: [AuthGuard],
    },

    { path: '**', redirectTo: PAGE_NOT_FOUND, pathMatch: 'full' },
    { path: PAGE_NOT_FOUND, component: PageNotFoundComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
