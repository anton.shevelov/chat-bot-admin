import { Component, OnInit, Input } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: '[app-order-details-categories]',
  templateUrl: './order-details-categories.component.html',
  styleUrls: ['./order-details-categories.component.scss']
})
export class OrderDetailsCategoriesComponent implements OnInit {
  @Input() detail: any = null;

  constructor() { }

  ngOnInit(): void {
  }
}
