import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductTypeTableHeaderComponent } from './product-type-table-header.component';

describe('ProductTypeTableHeaderComponent', () => {
  let component: ProductTypeTableHeaderComponent;
  let fixture: ComponentFixture<ProductTypeTableHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductTypeTableHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductTypeTableHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
