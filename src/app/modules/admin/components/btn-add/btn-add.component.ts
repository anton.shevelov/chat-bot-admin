import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    HostListener,
} from '@angular/core';

@Component({
    selector: 'app-btn-add',
    templateUrl: './btn-add.component.html',
    styleUrls: ['./btn-add.component.scss'],
})
export class BtnAddComponent implements OnInit {
    @Output() btnClick = new EventEmitter();
    @HostListener('click')
    onHostClick() {
        this.btnClick.emit(null);
    }
    constructor() {}

    ngOnInit(): void {}
}
