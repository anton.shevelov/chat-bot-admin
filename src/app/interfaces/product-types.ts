export interface ProductType {
    id?: number;
    name: string;
    readonly?: boolean;
}

export class DefaultProductType implements ProductType {
    id?: number;
    name = '';
    readonly = true;
    constructor(productType?: ProductType) {
        this.id = productType?.id || null;
        this.name = productType?.name || '';
        this.readonly = productType?.readonly || true;
    }
}
