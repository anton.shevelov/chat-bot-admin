export const SpinnerActionType = '[SPINNER]';
export interface SpinnerAction {
    visible: boolean;
}
export const MessageDialogType = '[MESSAGE_DIALOG]';

export interface MessageDialog {
    visible: boolean;
    dialogType: string;
    message: string;
}
