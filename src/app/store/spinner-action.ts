import { createAction, props } from '@ngrx/store';
import { SpinnerAction, SpinnerActionType } from '@actions';

export const Spinner = createAction(SpinnerActionType, props<SpinnerAction>());

export const defaultSpinnerState: SpinnerAction = {
    visible: false,
};
