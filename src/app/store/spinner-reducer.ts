import { createReducer, on, Action } from '@ngrx/store';
import { defaultSpinnerState, Spinner } from './spinner-action';
import { SpinnerAction } from '@actions';

const spinnerReducer = createReducer(
    defaultSpinnerState,
    on(Spinner, (state, { visible }) => ({ visible }))
);

export function spinner(state: SpinnerAction, action: Action) {
    return spinnerReducer(state, action);
}
